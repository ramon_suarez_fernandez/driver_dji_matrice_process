//////////////////////////////////////////////////////
//  parrotARDroneInps.h
//
//  Created on: Nov 12, 2015
//      Author: hriday bavle
//
//  Last modification on: Nov 19, 2015
//      Author: hriday bavle
//
//////////////////////////////////////////////////////

#include <iostream>
#include <math.h>
#include <cmath>

//tf messages
#include <tf/transform_datatypes.h>

//// ROS  ///////
#include "ros/ros.h"

#include "droneModuleROS.h"
#include "communication_definition.h"
#include "droneMsgsROS/droneAutopilotCommand.h"
#include "droneMsgsROS/battery.h"
#include "droneMsgsROS/droneYawRefCommand.h"
#include "droneMsgsROS/droneCommand.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/droneDAltitudeCmd.h"

//geometry msgs
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/PoseStamped.h"

//Navmsg
#include "nav_msgs/Odometry.h"

//mav_msgs_rotors
#include "mav_msgs_rotors/RollPitchYawrateThrust.h"
#include "eigen_conversions/eigen_msg.h"

//standard messages ROS
#include "std_msgs/Float64.h"
#include "tf/transform_datatypes.h"
#include "eigen_conversions/eigen_msg.h"
#include "Eigen/Core"
#include "Eigen/Geometry"
#include "tf_conversions/tf_eigen.h"
#include <cmath>

//Mavros
#include "mavros_msgs/AttitudeTarget.h"
#include "mavros_msgs/ActuatorControl.h"
#include "mavros_msgs/State.h"
#include "mavros/mavros_uas.h"
#include <pluginlib/class_list_macros.h>

#include <mavros/mavros_plugin.h>
#include <mavros/setpoint_mixin.h>

#include <dji_sdk/dji_drone.h>


#define ANGLE_TOLERANCE 0.0872664626    // 5 degrees in rad


/////////////////////////////////////////
// Class DroneCommand
//
//   Description
//
/////////////////////////////////////////
class DroneCommandROSModule : public DroneModule
{
    //Constructors and destructors
public:
    DroneCommandROSModule();
    ~DroneCommandROSModule();


    //Init and close
public:
    void init();
    void close();

    //Open
public:
    void open(ros::NodeHandle & nIn, std::string moduleName);
    void sendSetpoints();

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();

protected:
    //Subscribers
    ros::Subscriber ML_autopilot_command_subscriber;
    ros::Subscriber CommandSubs;
    ros::Subscriber rotation_angles_subscriber;
    ros::Subscriber battery_subscriber;
    ros::Subscriber hector_slam_subscriber;
    ros::Subscriber yaw_ref_subscriber;
    ros::Subscriber Ekf_yaw_subscriber;
    ros::Subscriber activate_aerostack_control_sub;
		ros::Subscriber daltitude_command_sub;		

    void MLAutopilotCommandCallback(const droneMsgsROS::droneAutopilotCommand& msg);
    void rotationAnglesCallback(const geometry_msgs::Vector3Stamped& msg);
    void batteryCallback(const droneMsgsROS::battery& msg);
    void hectorSlamCallback(const nav_msgs::Odometry& msg);
    void yawReferenceCallback(const droneMsgsROS::droneYawRefCommand& msg);
    void ekfyawCallback(const droneMsgsROS::dronePose& msg);
    void commandCallback(const droneMsgsROS::droneCommand::ConstPtr& msg);
    void activateAerostackControl(const std_msgs::Empty& msg);
		void daltitudeCommandCallback(const droneMsgsROS::droneDAltitudeCmd& msg);
    // Publishers
    ros::Publisher drone_CtrlInput_publisher;
    ros::Publisher drone_setpoint_attitude_throttle_publ;
    ros::Publisher mavros_yaw_command_publisher;

protected:		     
    int counter;
    bool activate_aerostack_control;

public:
    DJIDrone* drone;
    std::string mav_name;
    int droneSwarmId;
    std::vector<geometry_msgs::PoseStamped> poses;
    geometry_msgs::Quaternion orientation;
    Eigen::Quaterniond quaterniond, quaterniond_transformed;
    double roll_command, pitch_command, yaw_command, yaw_angle;
    double roll, pitch, yaw;
    double battery_percent;
    double yawRef, yaw_initial_mag, ekf_yaw;
    double timePrev, timeNow;
    bool update;
		double drone_command_daltitude_msg;

};
