//////////////////////////////////////////////////////
//  droneOuts.h
//
//  Created on: Nov 12, 2015
//      Author: Hriday Bavle
//
//  Last modification on: Nov 19, 2015
//      Author: Hriday Bavle
//
///////////////////////////////////////////////////////

//ROS
#include "ros/ros.h"

//math library
#include "math.h"

#include "droneModuleROS.h"
#include "communication_definition.h"

//IMU
#include "sensor_msgs/Imu.h"

//Odometry
#include "nav_msgs/Odometry.h"

//Rotaion Angles
#include "geometry_msgs/Vector3Stamped.h"
#include "geometry_msgs/TwistStamped.h"

//Battery
#include "droneMsgsROS/battery.h"

//Altitude
#include "droneMsgsROS/droneAltitude.h"
#include "sensor_msgs/Range.h"

//Speeds
#include "droneMsgsROS/vector2Stamped.h"

//Mavros_msgs
#include "mavros_msgs/BatteryStatus.h"
#include "mavros/mavros_uas.h"
#include <pluginlib/class_list_macros.h>
#include <cmath>

//tf messages
#include <tf/transform_datatypes.h>

//Eigen
#include "eigen_conversions/eigen_msg.h"

// low pass filter
#include "control/LowPassFilter.h"
#include "xmlfilereader.h"
#include "control/filtered_derivative_wcb.h"

#include <gazebo_msgs/ModelStates.h>

#include <Eigen/Dense>

#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "sensor_msgs/LaserScan.h"

//#define NoAltitudeFiltering
#define AltitudeFiltering

class RotationAnglesROSModule : public DroneModule
{

    //Publisher
protected:
    ros::Publisher RotationAnglesPubl;
    ros::Publisher RotationAnglesRadiansPub;
    ros::Publisher ImuDataPubl;
    bool publishRotationAnglesValue(const geometry_msgs::Vector3Stamped &RotationAnglesMsgs);
    bool publishImuData(const sensor_msgs::Imu &ImuData);
    ros::Time lastTimeRotation;


    //Subscriber
protected:
    ros::Subscriber RotationAnglesSubs;
   void rotationAnglesCallback(const sensor_msgs::Imu &msg);

    //RotationAngles msgs
protected:
//    geometry_msgs::Vector3Stamped RotationAnglesMsgs;


    //Constructors and destructors
public:
    RotationAnglesROSModule();
    ~RotationAnglesROSModule();


    //Init and close
public:
    void init();
    void close();

    //Open
 public:
    void open(ros::NodeHandle & nIn, std::string moduleName);

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();

public:
    std::string mav_name;
    int droneSwarmId;
    double roll,pitch,yaw;
    double roll_rad, pitch_rad, yaw_rad;
    Eigen::Quaterniond quaterniond;
    geometry_msgs::Quaternion quaternion;

};

//Battery Class
class BatteryROSModule : public DroneModule
{

    //Publisher
protected:
    ros::Publisher BatteryPubl;
    bool publishBatteryValue(const droneMsgsROS::battery &BatteryMsgs);


    //Subscriber
protected:
    ros::Subscriber BatterySubs;
    void batteryCallback(const mavros_msgs::BatteryStatus &msg);


    //Battery msgs
protected:
//    droneMsgsROS::battery BatteryMsgs;


    //Constructors and destructors
public:
    BatteryROSModule();
    ~BatteryROSModule();


    //Init and close
public:
    void init();
    void close();

    //Open
 public:
    void open(ros::NodeHandle & nIn, std::string moduleName);

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();


};

//---Altitude given by the lidarlite --------------//
class AltitudeROSModule : public DroneModule
{
//Publisher
protected:
    ros::Publisher AltitudePub;
    bool publishAltitude(const droneMsgsROS::droneAltitude altitudemsg);


    //Subscriber
protected:
    ros::Subscriber AltitudeSub;
    ros::Subscriber AltitudeFilteredSub;
    void publishAlitudeCallback(const sensor_msgs::Range &msg);
    void publishAltitudeFilteredCallback(const geometry_msgs::PoseStamped &msg);

protected:
    droneMsgsROS::droneAltitude AltitudeMsg;
    CVG_BlockDiagram::FilteredDerivativeWCB filtered_derivative_wcb;

    //Constructors and destructors
public:
    AltitudeROSModule();
    ~AltitudeROSModule();


    //Init and close
public:
    void init();
    void close();

    //Open
 public:
    void open(ros::NodeHandle & nIn, std::string moduleName);

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    std::string mav_name;
    int droneSwarmId;
    bool run();


};

//----------------Local Pose given by Mavros------------//

class LocaPoseROSModule : public DroneModule
{
    //Publisher
protected:
    ros::Publisher localPoseAltitudePublisher;
    bool publishLocalPoseAltitude(const droneMsgsROS::droneAltitude &altitudemsg);
    droneMsgsROS::droneAltitude AltitudeMsg;

    ros::Publisher localPoseSpeedsPublisher;
    ros::Publisher localPosePublisher;
    ros::Publisher localSpeedPublisher;
    bool publishLocalPoseSpeeds(const droneMsgsROS::vector2Stamped &speedsmsg);
    droneMsgsROS::vector2Stamped SpeedsMsg;
    droneMsgsROS::dronePose gazeboPoseMsg;
    droneMsgsROS::droneSpeeds gazeboSpeedsMsg;

    ros::Time lastTimePose;

    //Subscriber
protected:
    ros::Subscriber mavrosLocalPoseSubsriber;
    void localPoseAltitudeCallback(const nav_msgs::Odometry &msg);
    ros::Subscriber mavrosLocalSpeedsSubsriber;
    void localSpeedsCallback(const geometry_msgs::TwistStamped &msg);
    void localSpeedsCallbackGazebo(const nav_msgs::Odometry &msg);

protected:
    CVG_BlockDiagram::FilteredDerivativeWCB filtered_derivative_wcb;

    //Constructors and destructors
public:
    LocaPoseROSModule();
    ~LocaPoseROSModule();


    //Init and close
public:
    void init();
    void close();

    //Open
 public:
    void open(ros::NodeHandle & nIn, std::string moduleName);

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();

public:
    std::string mav_name;
    int droneSwarmId;

   // double roll,pitch,yaw;

};
