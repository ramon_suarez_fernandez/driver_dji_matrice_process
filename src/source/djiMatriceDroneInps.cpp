//Drone

#include "djiMatriceDroneInps.h"


using namespace DJI::onboardSDK;

////// DroneCommand ////////
DroneCommandROSModule::DroneCommandROSModule() :
    DroneModule(droneModule::active)
{
    init();
    return;
}

DroneCommandROSModule::~DroneCommandROSModule()
{
    return;
}

void DroneCommandROSModule::init()
{
    counter = 0;
		drone_command_daltitude_msg = 0.0;
    activate_aerostack_control = false;
    return;
}

void DroneCommandROSModule::close()
{
    return;
}



void DroneCommandROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //Configuration
    ros::param::get("~droneSwarmId", droneSwarmId);
    ros::param::get("~mav_name", mav_name);


    drone = new DJIDrone(nIn);

    //Publishers
    //drone_CtrlInput_publisher       = n.advertise<mav_msgs_rotors::RollPitchYawrateThrust>("/"+mav_name+std::to_string(droneSwarmId)+"/command/roll_pitch_yawrate_thrust", 1, true);
    
    //Subscribers
    CommandSubs                     = n.subscribe("command/high_level", 3, &DroneCommandROSModule::commandCallback, this);
    ML_autopilot_command_subscriber = n.subscribe("command/low_level", 1, &DroneCommandROSModule::MLAutopilotCommandCallback, this);
    battery_subscriber              = n.subscribe("battery",1, &DroneCommandROSModule::batteryCallback, this);
    activate_aerostack_control_sub  = n.subscribe("activate_aerostack_control", 1, &DroneCommandROSModule::activateAerostackControl, this);
		daltitude_command_sub						= n.subscribe("command/dAltitude",1, &DroneCommandROSModule::daltitudeCommandCallback, this);	

    //Flag of module opened
    droneModuleOpened=true;


    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

//Reset
bool DroneCommandROSModule::resetValues()
{
    return true;
}

//Start
bool DroneCommandROSModule::startVal()
{
    return true;
}

//Stop
bool DroneCommandROSModule::stopVal()
{
    return true;
}

//Run
bool DroneCommandROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}

void DroneCommandROSModule::yawReferenceCallback(const droneMsgsROS::droneYawRefCommand& msg){
    yawRef = msg.yaw;
    update = true;
}

void DroneCommandROSModule::daltitudeCommandCallback(const droneMsgsROS::droneDAltitudeCmd& msg)
{
	drone_command_daltitude_msg = msg.dAltitudeCmd;
}

#define KEEP_IN_RANGE(a, min, max) if (a < min) a = min; else if (a > max) a = max;

void DroneCommandROSModule::MLAutopilotCommandCallback(const droneMsgsROS::droneAutopilotCommand& msg)
{
    //Asynchronous module with only one callback
    if(!run())
        return;

    if(activate_aerostack_control)
    {

    //asking for control permission from dji sdk
    if(counter == 0)
    {
        if(drone->request_sdk_permission_control())
            counter++;
    }

    mav_msgs_rotors::RollPitchYawrateThrust attitude_msg;

    //attitude_msg.header.stamp = ros::Time::now();

    roll_command  = (+1)*msg.roll;
    pitch_command = (+1)*msg.pitch;
    yaw_command   = (+1)*msg.dyaw;

    //attitude_msg.pitch = pitch_command;
    //attitude_msg.roll = roll_command;
    //attitude_msg.yaw_rate = yaw_command;

    //attitude_msg.thrust.z = msg.thrust;
		

    //this control byte is based on dji sdk documentation
    //for more details see: https://developer.dji.com/onboard-sdk/documentation/appendix/index.html#control-mode-byte
    unsigned char control_type = 0xA;
    drone->attitude_control(control_type, roll_command, pitch_command, drone_command_daltitude_msg, yaw_command);
    }


    return;
}

void DroneCommandROSModule::batteryCallback(const droneMsgsROS::battery &msg)
{
    battery_percent = msg.batteryPercent;
    return;
}

void DroneCommandROSModule::commandCallback(const droneMsgsROS::droneCommand::ConstPtr& msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;
		
		if(activate_aerostack_control)
    {
	
    switch(msg->command)
    {
    case droneMsgsROS::droneCommand::TAKE_OFF:
        /* take off */
        if(drone->request_sdk_permission_control())
        {
        	if(drone->takeoff())				
          	break;
				}
    case droneMsgsROS::droneCommand::LAND:
        /* landing*/
        if(drone->landing())
        	{
						if(drone->release_sdk_permission_control())
							{
							 activate_aerostack_control = false;
               break;
							}
					}
    default:
        break;
    }

		}
    return;
}

void DroneCommandROSModule::activateAerostackControl(const std_msgs::Empty &msg)
{
    activate_aerostack_control = true;
}
